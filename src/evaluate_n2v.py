from node2vec import Node2Vec

import warnings
warnings.filterwarnings('ignore')

import networkx as nx
from sklearn.cluster import KMeans
from sklearn import metrics
from datetime import datetime

walk_length = 80
num_walks = 10
p = 1.0
q = 1.0
workers = 4

dimensions = 128
window = 10
min_count = 1
seed = 0

# count_embeddings function contains the node2vec's version now 
def count_embeddings(G,labels,dimensions,
                    walk_length,num_walks,p,q,
                    window,min_count,
                    seed,workers):
    n2v = Node2Vec(G, 
               dimensions = dimensions, 
               walk_length = walk_length,
               num_walks = num_walks,
               p = p,
               q = q,
               workers = workers
              )
    model = n2v.fit(window = window, 
                    min_count = min_count,
                    seed = seed
                   )
    model_nodes = list(model.wv.vocab)

    return model,model_nodes
    
def count_score(labels, clusters):
    adj_score = metrics.adjusted_rand_score(labels,clusters)
    return adj_score

def make_graph(labels, edges):
    G = nx.Graph()
    G.add_nodes_from(labels['node'])

    for i, row in edges.iterrows():
        G.add_edge(row['from'],row['to'], weight=1)
    return G

def draw_g(G):
    pos = nx.spring_layout(G)
    nx.draw(G, pos=pos, with_labels=True, seed=seed)

def write_to_file(res,param):
    now = datetime.now()
    current_time = now.strftime('%Y%m%d_one_eighth_'+param)
    filename = '../results/' + current_time + '.csv'
    res.to_csv(filename)

def evaluate(G,labels,
        dimensions=dimensions,
        walk_length=walk_length,num_walks=num_walks,p=p,q=q,
        window=window,min_count=min_count):
    model,model_nodes = count_embeddings(G,labels,dimensions,
                            walk_length,num_walks,p,q,
                            window,min_count,seed,workers)

    known_labels = []
    for i in model.wv.index2word:
        known_labels.append(labels.loc[int(i)]['label'])

    n_clusters = len(labels['label'].unique())
    km = KMeans(n_clusters=n_clusters,random_state=seed).fit_predict(model.wv.vectors)
    km_map = dict(zip(model.wv.index2word, km))

    return count_score(known_labels, km)
