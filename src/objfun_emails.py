from objfun import ObjFun

import networkx as nx
import numpy as np
import pandas as pd
from sklearn import cluster, linear_model, metrics
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import time
# Silence perf warning
import warnings

import nodevectors as graph2vec
from node2vec import Node2Vec
import csrgraph

warnings.simplefilter("ignore")

WALKLEN = 80 # l in paper
EPOCH = 10 # r in paper for EPOCH in [6,8,10,12,14,16,18,20]:
N_WEIGHT = 1. # for N_WEIGHT in [0.25, 0.5, 1., 2., 4]: q in paper
R_WEIGHT = 1. # for R_WEIGHT in [0.3, 1., 3.]: p in paper
WINDOW = 10 # k in paper for WINDOW in [10]:
EMBED_SIZE = 128  # d in paper
NS_EXP = 0.75  # default, not in paper
NEGATIVE = 5

class n2v(ObjFun):

    """
    1-dimensional demo task from the first exercise
    """

    def __init__(self):
        """
        Hard-coded initialization
        """
        fstar = -1
        a = 0
        b = 8
        G, labels = self.make_snap()

        y = labels.label
        n_clusters = y.nunique()

        labels_dic = {}
        for x, row in labels.iterrows():
            labels_dic[row[0]] = row[1]

        X_train, X_test, y_train, y_test = train_test_split(
            labels.node, labels.label, test_size=0.10,
            random_state=33)

        super().__init__(fstar, a, b, G, labels, n_clusters, labels_dic,
                         X_train, X_test, y_train, y_test)

    def make_snap(self):
        """
        Graph from university emails, clustered by departments
        Data from http://snap.stanford.edu/data/email-Eu-core.html
        Edge list Format
        """
        edges = pd.read_csv('../data/emails/email-Eu-core.txt', header=None, sep=' ')
        edges.columns = ['src', 'dest']
        # cluster labels per node
        labels = pd.read_csv('../data/emails/email-Eu-core-department-labels.txt', header=None, sep=' ')
        labels.columns = ['node', 'cluster']
        G = nx.Graph()
        G.add_edges_from([(t.src, t.dest) for t in edges.itertuples()])
        return G, pd.DataFrame({'node': list(G), 'label': labels.cluster})

    def generate_point(self):
        """
        Random point generator
        :return: random point from the domain
        """
        return round(np.random.uniform(0,8), 2)

    def get_neighborhood(self, x, d):
        """
        Solution neighborhood generating function
        :param x: point
        :param d: diameter of the neighbourhood
        :return: list of points in the neighborhood of the x
        """
        left = [x for x in np.arange(x-0.1, x - d - 0.1, -1, dtype=float) if x >= self.a]
        right = [x for x in np.arange(x+0.1, x + d + 0.1, dtype=float) if x < self.b]
        if np.size(left) == 0:
            return right
        elif np.size(right) == 0:
            return left
        else:
            return np.concatenate((left, right))

    def evalClusteringOnLabels(self, clusters, groupLabels, verbose=True):
        results = []
        results.append(metrics.f1_score(groupLabels, clusters, average='micro'))
        results.append(metrics.adjusted_rand_score(clusters, groupLabels))
        results.append(metrics.fowlkes_mallows_score(clusters, groupLabels))
        if verbose:
            print("f1_score:   {0:.2f}".format(results[0]))
            print("adj. RAND score: {0:.2f}".format(results[1]))
            print("F-M score:       {0:.2f}".format(results[2]))
        return np.array(results)

    def to_X(self, node_labels, embedder):
        """
        Takes a series of node names and returns matrix of embeddings
        """
        X = pd.DataFrame.from_records(
            node_labels.astype(type(list(self.graph)[0])).apply(embedder.predict).values)
        return X

    # faster implementation with the CSR matrices
    def evaluate(self, x):
        """
        Objective function evaluating function
        :param x: q parameter's value 
        :return: f1_micro
        """
        embedder = graph2vec.Node2Vec(
            walklen=WALKLEN,
            epochs=EPOCH,
            return_weight=x,
            neighbor_weight=N_WEIGHT,
            n_components=EMBED_SIZE,
            w2vparams={'window': WINDOW,
                       'negative': NEGATIVE,
                       'iter': 5,
                       'ns_exponent': NS_EXP,
                       'batch_words': 128}
        )
        embedder.fit(self.graph)

        X_full = self.to_X(self.labels.node, embedder=embedder)
        assigned_clusters = cluster.KMeans(
            n_clusters=self.n_clusters,
            random_state=25
        ).fit(X_full).labels_

        # labels sorting: so assigned_clusters and sorted_labels are in the same order of nodes
        # sorted_labels = []
        # nodes_order = []
        # nodes = list(embedder.model.wv.vocab)
        # for i, node in enumerate(nodes):
        #     nodes_order.append(int(node))
        #     sorted_labels.append(self.labels_dic[int(node)])

        # x = self.evalClusteringOnLabels(assigned_clusters, self.labels.label)
        f1_micro = metrics.f1_score(self.labels.label, assigned_clusters, average='micro')

        return -f1_micro  # negative altitude, because we are minimizing (to be consistent with other obj. functions)

    # very slow implementation of node2vec
    def evaluate_slow(self, x):
        """
        Objective function evaluating function
        :param x: point
        :return: objective function value
        """
        node2vec = Node2Vec(self.graph, dimensions=10, walk_length=80, num_walks=10, p=x, q=1.)
        model = node2vec.fit(window=10, min_count=1)

        # clustering
        X = model[model.wv.vocab]
        kmeans = cluster.KMeans(n_clusters=self.n_clusters, random_state=25)
        kmeans.fit(X)
        assigned_clusters = kmeans.labels_

        # labels sorting: so assigned_clusters and sorted_lables are in the same order of nodes
        sorted_lables = []
        nodes_order = []
        nodes = list(model.wv.vocab)
        for i, node in enumerate(nodes):
            nodes_order.append(int(node))
            sorted_lables.append(self.lables_dic[int(node)])

        # scores
        # adj_rand_score = adjusted_rand_score(assigned_clusters, sorted_lables)
        f1_micro = f1_score(sorted_lables, assigned_clusters, average='micro')

        # return adj_rand_score, f1_micro
        return -f1_micro

    def evaluate_loop(self, x):
        """
        Objective function evaluating function
        :param x: point
        :return: objective function value
        """
        for WALKLEN in [80]:  # l in paper
            for EPOCH in [10]:  # r in paper for EPOCH in [6,8,10,12,14,16,18,20]:
                for N_WEIGHT in [1.]:  # for N_WEIGHT in [0.25, 0.5, 1., 2., 4]: q in paper
                    for R_WEIGHT in [x]:  # for R_WEIGHT in [0.3, 1., 3.]: p in paper
                        for WINDOW in [10]:  # k in paper for WINDOW in [10]:
                            for EMBED_SIZE in [128]:  # d in paper
                                for NS_EXP in [0.75]:  # default, not in paper
                                    for NEGATIVE in [5]:  # default, not in paper
                                        # start_t = time.time()
                                        embedder = graph2vec.Node2Vec(
                                            walklen=WALKLEN,
                                            epochs=EPOCH,
                                            return_weight=R_WEIGHT,
                                            neighbor_weight=N_WEIGHT,
                                            n_components=EMBED_SIZE,
                                            w2vparams={'window': WINDOW,
                                                       'negative': NEGATIVE,
                                                       'iter': 5,
                                                       'ns_exponent': NS_EXP,
                                                       'batch_words': 128}
                                        )
                                        embedder.fit(self.graph)
                                        # train_t = time.time()
                                        # print(f"Fit Embedder: {time.time() - start_t:.2f}")
                                        logit = linear_model.LogisticRegressionCV(cv=5, scoring='f1_macro',
                                                                                  max_iter=3000,
                                                                                  solver='lbfgs',
                                                                                  multi_class='ovr')
                                        X_full = self.to_X(self.labels.node, embedder=embedder)
                                        scaler = StandardScaler().fit(X_full)
                                        logit.fit(scaler.transform(self.to_X(self.X_train, embedder=embedder)), self.y_train)
                                        score = logit.scores_[1].mean(axis=0).max()
                                        # print(f"Trained: {time.time() - start_t:.2f}")
                                        # print(f'best CV score: {score :.4f}')
                                        test_score = metrics.f1_score(
                                            y_true=self.y_test,
                                            y_pred=logit.predict(scaler.transform(self.to_X(self.X_test, embedder=embedder))),
                                            average='micro'
                                        )
                                        # print(f"test score: {test_score :.4f}")

                                        assigned_clusters = cluster.KMeans(
                                            n_clusters=self.n_clusters,
                                            random_state=25
                                        ).fit(X_full).labels_

                                        x = self.evalClusteringOnLabels(assigned_clusters, self.labels.label)
                                        # print("-------------------\n\n")
                                        #
                                        # res = pd.DataFrame(
                                        #     columns=['params', 'F1', 'F1_test', 'F1_micro', 'RAND', 'F-M'])
                                        # res = res.append({
                                        #     'params': {
                                        #         'walklen': WALKLEN,
                                        #         'epochs': EPOCH,
                                        #         'return_weight': R_WEIGHT,
                                        #         'neighbor_weight': N_WEIGHT,
                                        #         'window': WINDOW,
                                        #         'size': EMBED_SIZE,
                                        #         'negative': NEGATIVE,
                                        #         'iter': EPOCH,
                                        #         'ns_exponent': NS_EXP,
                                        #         'batch_words': 128,
                                        #     },
                                        #     # 'traintime': train_t - start_t,
                                        #     'F1': score,
                                        #     'F1_test': test_score,
                                        #     'F1_micro': x[0],
                                        #     'RAND': x[1],
                                        #     'F-M': x[2]},
                                        #     ignore_index=True)

        return -x[0]  # negative altitude, because we are minimizing (to be consistent with other obj. functions)
