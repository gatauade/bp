class ObjFun(object):

    """
    Generic objective function super-class
    """

    def __init__(self, fstar, a, b, G, labels,n_clusters,labels_dic,
                 X_train, X_test, y_train, y_test):
        """
        Default initialization function that sets:
        :param fstar: f^* value to be reached (can be -inf)
        :param a: domain lower bound vector
        :param b: domain upper bound vector
        """
        self.fstar = fstar
        self.a = a
        self.b = b
        self.graph = G
        self.labels = labels
        self.n_clusters = n_clusters
        self.labels_dic = labels_dic
        self.X_train = X_train
        self.X_test = X_test
        self.y_train = y_train
        self.y_test = y_test

    def get_fstar(self):
        """
        Returns f^*
        :return: f^* value
        """
        return self.fstar

    def get_bounds(self):
        """
        Returns domain bounds
        :return: list with lower and upper domain bound
        """
        return [self.a, self.b]

    def generate_point(self):
        """
        Random point generator placeholder
        :return: random point from the domain
        """
        raise NotImplementedError("Objective function must implement its own random point generator")

    def get_neighborhood(self, x):
        """
        Solution neighborhood generating function placeholder
        :param x: point
        :return: list of points in the neighborhood of the x
        """
        raise NotImplementedError("Objective function must implement its own neighborhood generator")

    def evaluate(self, x):
        """
        Objective function evaluating function placeholder
        :param x: point
        :return: objective function value
        """
        raise NotImplementedError("Objective function must implement its own evaluation")
