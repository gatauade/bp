from objfun import ObjFun
import numpy as np
import pandas as pd
import networkx as nx

from node2vec import Node2Vec
from sklearn import cluster
from sklearn.metrics import adjusted_rand_score
from sklearn.metrics import f1_score


class Node2VecFun(ObjFun):
    """
    node2vec evaluation objective function
    """

    def __init__(self, data_path, groups_path):
        """
        Hard coded initialization for p or q parametres,
        which take values from 0 to 2
        (not sure about that numbers)
        """
        edges, groups, lables_dic = self.data_reading(data_path, groups_path)
        graph = self.graph_const(edges)
        fstar = -1
        a = 0
        b = 2
        super().__init__(fstar, a, b, edges, groups, lables_dic, graph)

    def generate_point(self):
        """
        Random point generator
        :return: random point from the domain
        rounded to 0.1
        """
        return round(np.random.uniform(0.1, 2.0), 1)

    def get_neighborhood(self, x, d):
        """
        Solution neighborhood generating function
        :param x: point
        :param d: diameter of the neighbourhood
        :return: list of points in the neighborhood of the x
        """
        left = [x for x in np.arange(x-1, x - d - 1, -1, dtype=int) if x >= 0]
        right = [x for x in np.arange(x+1, x + d + 1, dtype=int) if x < 800]
        if np.size(left) == 0:
            return right
        elif np.size(right) == 0:
            return left
        else:
            return np.concatenate((left, right))

    def data_reading(self, data_path, groups_path):
        """data reading"""
        edges = pd.read_csv(data_path, sep=' ')
        edges.columns = ['from', 'to']

        groups = pd.read_csv(groups_path, sep=' ', header=None)
        groups.columns = ['node', 'department']
        groups = groups.sort_index()

        # groups dictionary: node_number -> groups number
        departm_dic = {}
        for x, row in groups.iterrows():
            departm_dic[row[0]] = row[1]

        return edges, groups, departm_dic

    def graph_const(self, edges):

        G = nx.Graph()
        G.add_nodes_from(edges['from'])
        G.add_nodes_from(edges['to'])

        for index, row in edges.iterrows():
            G.add_edge(row["from"], row["to"], weight=1)
        return G

    def evaluate(self, p):
        """
        Objective function evaluating function
        :param x: point
        :return: objective function value
        """
        node2vec = Node2Vec(self.graph, dimensions=10, walk_length=80, num_walks=10, p=p, q=1)
        model = node2vec.fit(window=10, min_count=1)

        # clustering
        X = model[model.wv.vocab]
        kmeans = cluster.KMeans(n_clusters=42, random_state=25)
        kmeans.fit(X)
        assigned_clusters = kmeans.labels_

        # labels sorting: so assigned_clusters and sorted_lables are in the same order of nodes
        sorted_lables = []
        nodes_order = []
        nodes = list(model.wv.vocab)
        for i, node in enumerate(nodes):
            nodes_order.append(int(node))
            sorted_lables.append(self.lables_dic[int(node)])

        # scores
        # adj_rand_score = adjusted_rand_score(assigned_clusters, sorted_lables)
        f1_micro = f1_score(sorted_lables, assigned_clusters, average='micro')

        # return adj_rand_score, f1_micro
        return -f1_micro

