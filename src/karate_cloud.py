import pandas as pd
import numpy as np
import networkx as nx

from newNode2vec import Node2Vec
import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')

from sklearn import cluster
from sklearn.metrics.cluster import adjusted_rand_score
from sklearn.metrics import f1_score

import multiprocessing
# CORES = 4
CORES = multiprocessing.cpu_count()


def calc_scores(G, num_clusters, walk_length=80, num_walks=10, p=1, q=1):
    print('walk_length:', walk_length, ', num_walks:', num_walks, ', p:', p, ', q:', q)

    # node2vec
    node2vec = Node2Vec(G, dimensions=128, walk_length=walk_length, num_walks=num_walks, p=p, q=q)
    model = node2vec.fit(size=8, window=2, sg=1, iter=1, min_count=1, workers=CORES-1, seed=1)

    # clustering
    X = model[model.wv.vocab]
    kmeans = cluster.KMeans(n_clusters=num_clusters, random_state=25)
    kmeans.fit(X)
    assigned_clusters = kmeans.labels_

    # labels sorting: so assigned_clusters and sorted_lables are in the same order of nodes
    sorted_lables = []
    nodes_order = []
    nodes = list(model.wv.vocab)
    for i, node in enumerate(nodes):
        nodes_order.append(int(node))
        sorted_lables.append(0 if G.node[int(node)]['club'] == 'Mr. Hi' else 1)

    # scores
    adj_rand_score = adjusted_rand_score(assigned_clusters, sorted_lables)
    f1_micro = f1_score(sorted_lables, assigned_clusters, average='micro')
    f1_macro = f1_score(sorted_lables, assigned_clusters, average='macro')

    return adj_rand_score, f1_micro, f1_macro

def run():
    G_karate = nx.karate_club_graph()
    clusters_2 = 2

    from datetime import datetime
    now = datetime.now()
    current_time = now.strftime('%Y%m%d-%H%M%S_karate_')
    filename = '../results/' + current_time + 'results.csv'

    with open(filename, 'a') as f:
        f.write('walk_length,num_walks,p,q, adj_rand_score,f1_micro, f1_macro\n')  # the header of the results table

    # walk_length, num_walks, p, q, clusters_num = 80, 10, 1, 1, 39  # default parametres
    # for i in range(30, 120, 10):  # walk_length = 80
    #     for j in range(5, 30, 1):  # num_walks = 10
    #         for p in np.arange(0.1, 3.9, 0.1):
    #             for q in np.arange(0.1, 3.9, 0.1):
    #                 adj_rand_score, f1_micro, f1_macro = calc_scores(G_karate, clusters_2, i, j, p, q)
    #                 # write results into file
    #                 with open(filename, 'a') as f:
    #                     f.write('{},{},{},{},{},{},{}\n'.format(i, j, p, q, adj_rand_score, f1_micro, f1_macro))
    #
    # f.close()

    walk_length, num_walks, p, q, clusters_num = 80, 10, 1, 1, 39  # default parametres
    for p in np.arange(0.1, 1.2, 0.2):
        for q in np.arange(0.1, 1.2, 0.2):
            adj_rand_score, f1_micro, f1_macro = calc_scores(G_karate, clusters_2, 70, 24, p, q)
            # write results into file
            with open(filename, 'a') as f:
                f.write('{},{},{},{},{},{},{}\n'.format(70, 24, p, q, adj_rand_score, f1_micro, f1_macro))

    f.close()


run()